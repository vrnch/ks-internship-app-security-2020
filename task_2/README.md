# Task 2 [[solution]](task_2.py)
Расшифровать входные данные. В данном случае используется асимметричный подход - по схеме ECDH (Elliptic Curve Diffie-Hellman)
при помощи пары закрытого ключа и открытого ключа генерируется ключ1, из которого с помощью HKDF получается произвольный ключ2,
а он в свою очередь используется в симметричном шифровании с помощью AES. Пароль для private key у всех один и тот же - KeepSolid2020

В алгоритме DH происходит derive (получение производного ключа) из public_key и private_key. Производный ключ является
одним и тем же для получателя и отправителя, поэтому он называется shared_secret или shared_key

**Данные**:
```
password: KeepSolid2020
-----BEGIN ENCRYPTED PRIVATE KEY-----
MIHsMFcGCSqGSIb3DQEFDTBKMCkGCSqGSIb3DQEFDDAcBAgPdZj8vFNQhAICCAAw
DAYIKoZIhvcNAgkFADAdBglghkgBZQMEASoEEMS386uw+LPks9H4l7JAKx8EgZCu
ttUt+gxuo1Mp8XtslPf02CoEVyuAZHd3TtjAYAA94lSiYhV9jsdopTe6+KUkvuVB
NySE+YUD8nHF+ITC5fZkVehdsD5R5qKQsNgB7AykzOsyBH/nceSYEHekrE1lw8tm
67SX1pA3gTSEiFeQU1p3KOCSbiKoSkNBGIbBg17Se/ENKQmtPemZGwljVKz9vR4=
-----END ENCRYPTED PRIVATE KEY-----
{
   "algorithm":"ECDH",
   "ecdh_curve":"secp384r1",
   "hkdf_params":"HKDF-SHA-512$Q0RjZUhQNFhLT3V5YjBPWEFsWFZSdUN2ZGY4ckdzM21WbG84OTUrakhXMA==",
   "public_key":"-----BEGIN PUBLIC KEY-----\nMEYwEAYHKoZIzj0CAQYFK4EEACIDMgACOETq5BpvxBYQrtmUS5lP+OPtIRiW35gj\njzl8Ek/LRb9p2ESekkwWAm+fIvAMKzjx\n-----END PUBLIC KEY-----\n",
   "encrypted_data":"AES256-GCM$Ypet4N9Zza5TrvV41Krfjw==$A1ul0DzfBbSPZ9D6TGW2eE5HI0B7tuJxltWWItZL5U7SAW3sf2nqzOb6I3IrJ0CsXg2kbxKTvR9XvMg=$ahuBp9aYIlgUWCOJ8BB/sA==",
   "type":"asymmetric_encryption"
}
```

алгоритм_шифрования$инициализационный_вектор$зашифрованные_данные$тег_проверки_целостности


