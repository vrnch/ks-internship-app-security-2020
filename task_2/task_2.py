import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers import (
    Cipher, algorithms, modes
)
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import ec


def decrypt(derived_key, cipher_text, iv, tag):
    decryptor = Cipher(
        algorithms.AES(derived_key),
        modes.GCM(iv, tag),
        backend=default_backend()
    ).decryptor()

    return decryptor.update(cipher_text) + decryptor.finalize()


serialized_private = b"""-----BEGIN ENCRYPTED PRIVATE KEY-----
MIHsMFcGCSqGSIb3DQEFDTBKMCkGCSqGSIb3DQEFDDAcBAgPdZj8vFNQhAICCAAw
DAYIKoZIhvcNAgkFADAdBglghkgBZQMEASoEEMS386uw+LPks9H4l7JAKx8EgZCu
ttUt+gxuo1Mp8XtslPf02CoEVyuAZHd3TtjAYAA94lSiYhV9jsdopTe6+KUkvuVB
NySE+YUD8nHF+ITC5fZkVehdsD5R5qKQsNgB7AykzOsyBH/nceSYEHekrE1lw8tm
67SX1pA3gTSEiFeQU1p3KOCSbiKoSkNBGIbBg17Se/ENKQmtPemZGwljVKz9vR4=
-----END ENCRYPTED PRIVATE KEY-----\n"""

serialized_public = b"-----BEGIN PUBLIC KEY-----\nMEYwEAYHKoZIzj0CAQYFK4EEACIDMgACOETq5BpvxBYQrtmUS5lP+OPtIRiW35gj\njzl8Ek/LRb9p2ESekkwWAm+fIvAMKzjx\n-----END PUBLIC KEY-----\n"

loaded_public_key = serialization.load_pem_public_key(
    serialized_public,
    backend=default_backend()
)
loaded_private_key = serialization.load_pem_private_key(
    serialized_private,
    password=b'KeepSolid2020',
    backend=default_backend()
)

iv = base64.b64decode(b'Ypet4N9Zza5TrvV41Krfjw==')
encrypted_data = base64.b64decode(b'A1ul0DzfBbSPZ9D6TGW2eE5HI0B7tuJxltWWItZL5U7SAW3sf2nqzOb6I3IrJ0CsXg2kbxKTvR9XvMg=')
tag = base64.b64decode(b'ahuBp9aYIlgUWCOJ8BB/sA==')
salt = base64.b64decode(b'Q0RjZUhQNFhLT3V5YjBPWEFsWFZSdUN2ZGY4ckdzM21WbG84OTUrakhXMA==')

# key1
shared_key = loaded_private_key.exchange(ec.ECDH(), loaded_public_key)

# key2
derived_key = HKDF(
    algorithm=hashes.SHA512(),
    length=32,
    salt=salt,
    info=None,
    backend=default_backend()
).derive(shared_key)

decrypted_data = decrypt(derived_key, encrypted_data, iv, tag)
print("Decrypted data:", decrypted_data.decode('utf-8'))
