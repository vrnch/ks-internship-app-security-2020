# Task 1 [[solution]](task_1.py)
Расшифровать входные данные. Последний символ пароля неизвестен, его необходимо подобрать и замерять время на подбор пароля.
Пароль пропускается через PBKDF и результат используется в качестве ключа шифрования данных.
После этого тем же паролем зашифровать в том же формате строку "Hello, World! My name is {имя}", представленную в кодировке UTF8.

**Данные**:  
Password: KeepSolid2020_?  
Salt: PBKDF2$SHA512$262144$eOAKk9UgyAA0bppNB3pTA2YP0METL2MXMDHHxhdX6xw=  
Encrypted Data:AES256-GCM$NvX3iLtqHBtXtldnMV1nYg==$lSJTq0StpAy6vWqA3yJEYAnJVcQMnLGEiahaJRWDzQR+d/L1OoLLmZ8ECoGWW31XwqRB4nYIo4DEOusLbUC3nQ==$rRc710PNrEhK5gj3DJOdQA==

Соль в формате алгоритм_pbkdf$хеш-функция$количество_итераций$сама_соль.  
Зашифрованные данные в формате алгоритм_шифрования$инициализационный_вектор$зашифрованные_данные$тег_проверки_целостности.


