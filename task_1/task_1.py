import os
import base64
import time
from string import printable as ascii_set
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.ciphers import (
    Cipher, algorithms, modes
)

password = b'KeepSolid2020_'
iterations = 262144
salt = base64.b64decode(b'eOAKk9UgyAA0bppNB3pTA2YP0METL2MXMDHHxhdX6xw=')
iv = base64.b64decode(b'NvX3iLtqHBtXtldnMV1nYg==')
encrypted_data = base64.b64decode(
    b'lSJTq0StpAy6vWqA3yJEYAnJVcQMnLGEiahaJRWDzQR+d/L1OoLLmZ8ECoGWW31XwqRB4nYIo4DEOusLbUC3nQ==')
tag = base64.b64decode(b'rRc710PNrEhK5gj3DJOdQA==')


def derive(key):
    pbkdf = PBKDF2HMAC(
        algorithm=hashes.SHA512(),
        length=32,
        salt=salt,
        iterations=iterations,
        backend=default_backend()
    )
    return pbkdf.derive(key)


def decrypt(derived_key, cipher_text, iv, tag):
    decryptor = Cipher(
        algorithms.AES(derived_key),
        modes.GCM(iv, tag),
        backend=default_backend()
    ).decryptor()

    return decryptor.update(cipher_text) + decryptor.finalize()


def encrypt(derived_key, plain_text):
    iv = os.urandom(24)

    encryptor = Cipher(
        algorithms.AES(derived_key),
        modes.GCM(iv),
        backend=default_backend()
    ).encryptor()
    cipher_text = encryptor.update(plain_text) + encryptor.finalize()
    return b'AES256-GCM$' + base64.b64encode(iv) + b'$' + base64.b64encode(cipher_text) + b'$' + base64.b64encode(
        encryptor.tag)


decrypted_data = b''
correct_guess = b''
derived_correct_guess = b''

start_time = time.time()

for i in ascii_set:
    try:
        guess = b''
        guess += password
        guess += bytes(i, 'utf-8')

        derived_correct_guess, correct_guess = derive(guess), guess
        decrypted_data = decrypt(derived_correct_guess, encrypted_data, iv, tag)
        break
    except:
        continue

end_time = time.time()

print("Guessed password:", correct_guess.decode('utf-8'))
print("Decrypted message:", decrypted_data.decode('utf-8'))
print("Elapsed time:", end_time - start_time)

encrypted_input_data = encrypt(derived_correct_guess, b"Hello, World! My name is Masha")
print("Encrypted message:", encrypted_input_data)
