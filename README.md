# ks-internship-app-security-2020
This repo includes solutions to the elective tasks from an internship at KeepSolid Inc.
* [Task 1](task_1/README.md)
* [Task 2](task_2/README.md)
* [Task 3](task_3/README.md)
 